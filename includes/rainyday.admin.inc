<?php

/**
 * @file
 * Functionality and helper functions for RainyDay administration.
 */

/**
 * Settings form for module rainyday.
 */
function rainyday_settings_form() {
  $themes = array();
  foreach (list_themes() as $theme) {
    $themes[$theme->name] = $theme->name;
  }

  $form['rainyday_themes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Themes'),
    '#description' => t('Select themes for rainyday.'),
    '#options' => $themes,
    '#default_value' => variable_get('rainyday_themes', array()),
  );

  $form['rainyday_image'] = array(
    '#title' => t('Background image'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://',
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg png jpeg'),
    ),
    '#default_value' => variable_get('rainyday_image', ''),
    '#description' => t(
      'Using !default_image where Background image is empty.',
      array(
        '!default_image' => l(
          t('default image'),
          RAINYDAY_DEFFAULT_IMAGE,
          array('attributes' => array(
            'target' => '_blank',
            ),
          )
        ),
      )
    ),
  );

  $form['rainyday_selectors'] = array(
    '#title' => t('Content selectors'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rainyday_selectors', '#skip-link, #navbar, .main-container'),
    '#description' => t("Parent content selectors for set relative position. Separated by <bold>','</bold> symbol."),
  );

  $form['rainyday_minified'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use minified js.'),
    '#description' => t("se minified js. Path in libraries './dist/rainyday.min.js'"),
    '#default_value' => variable_get('rainyday_minified', FALSE),
  );

  return system_settings_form($form);
}

/**
 * Submit for form rainyday_settings_form.
 */
function rainyday_settings_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['rainyday_image'])) {
    // Load the file via file.fid.
    $file = file_load($form_state['values']['rainyday_image']);
    // Change status to permanent.
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'rainyday', 'rainyday_image', $GLOBALS['user']->uid);
  }
}
