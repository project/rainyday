RainyDay

This module add HTML5 canvas to render an animation of raindrops falling
on a glass surface as background.

Module based on rainyday.js scripts
https://maroslaw.github.io/rainyday.js/

Install.
1. Download rainyday.js form GitHub https://github.com/maroslaw/rainyday.js
   and save it in 'sites/all/libraries/rainyday/'
2. Enable module
3. Configure on page admin/config/development/rainyday
