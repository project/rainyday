/*
 *@file
 * Js file for RainyDay module.
 */

(function ($) {
    Drupal.behaviors.rainyday = {
        attach: function (context, settings) {
            $('body', context).once('rainyday', function () {
                var rainydayImg = $('<img id="background-rainyday" alt="" src="' + Drupal.settings.rainyday.background + '" crossOrigin="anonymous" />');
                $(this).append(rainydayImg);
                $(Drupal.settings.rainyday.selectors).hide();
            });

            $('#background-rainyday').bind('load', function() {
                var engine = new RainyDay({
                    image: this
                });
                engine.rain([[1, 2, 8000]]);
                engine.rain([[3, 3, 0.88], [5, 5, 0.9], [6, 2, 1]], 100);

                $(Drupal.settings.rainyday.selectors).addClass('rainy-show').show();
            });
        }
    };
})(jQuery);